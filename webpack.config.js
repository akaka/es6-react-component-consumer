module.exports = {
  entry: ['./eventing.js'],
  output: {
    path: './',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: 'style-loader!css-loader'},
      {test: /\.js$/, loader: "babel-loader"},
      {test: /\.woff$/, loader: 'url?limit=100000'},
      {test: /\.eot$/, loader: 'url?limit=100000'},
      {test: /\.svg$/, loader: 'url?limit=100000'},
      {test: /\.ttf$/, loader: 'url?limit=100000'},
      {test: /\.json$/, loader: 'json'}
    ]
  }
};