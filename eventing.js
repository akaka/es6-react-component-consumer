var Cards = require('es6-react-component').default;

var cardContainer = new Cards('.container');
console && console.info(cardContainer.toString());

function init() {

  var eventedElementId = 'eventedElement',
      eventedElement = document.getElementById(eventedElementId),
      foucTarget = document.getElementsByClassName('no-fouc')[0];

  foucTarget ? foucTarget.className = '' : null;

  // Listen for any events coming out of evented components
  eventedElement.addEventListener('card-comment', function(e) {
    console.info(`${e.detail.username} says "${e.detail.comment}"`);
  });

  // Data in detail object would come from a service endpoint

  var initComponent1 = new CustomEvent('initCard', {
      detail: {
          eventedElem: eventedElementId,
          targetElem: "div1",
          userId: "abc",
          username: "aaron.kaka",
          bio: "This is Aaron's bio.",
          avatar: "images/avatar.png"
      }
  });
  eventedElement.dispatchEvent(initComponent1);

  var initComponent2 = new CustomEvent('initCard', {
      detail: {
          eventedElem: eventedElementId,
          targetElem: "div2",
          userId: "def",
          username: "joe.schmo",
          bio: "This is Joe's bio.",
          avatar: "images/wired.jpg"
      }
  });
  eventedElement.dispatchEvent(initComponent2);

  var initComponent3 = new CustomEvent('initCard', {
      detail: {
          eventedElem: eventedElementId,
          targetElem: "div3",
          userId: "ghi",
          username: "suzie.q",
          bio: "This is Suzie's bio.",
          avatar: "images/suzieq.jpg"
      }
  });
  eventedElement.dispatchEvent(initComponent3);

  // Outside the evented group
  var initComponent4 = new CustomEvent('initCard', {
      detail: {
          // NO eventedElem
          targetElem: "div4",
          userId: "jkl",
          username: "outsider.card",
          bio: "This card does not belong to the evented group of cards."
          // NO avatar
      }
  });
  document.getElementById('outsider').dispatchEvent(initComponent4);

}

window.onload = init;