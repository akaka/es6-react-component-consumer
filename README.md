## What is this?

A proof-of-concept consumer of the [es6-react-component](https://www.npmjs.com/package/es6-react-component) npm module.

## Toolchain

Install [NodeJS](http://nodejs.org/download/). This automatically installs npm.

## Consume

After cloning this repo:

    cd es6-react-component-consumer
    npm install
    
Then you **must build** the required bundle.

    npm run build

Open demo.html in your fave browser - no server required!